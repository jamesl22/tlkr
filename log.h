/* log.h - Part of TLKR, the decentralised messaging system
   Copyright (C) 2015  James Lovejoy  jameslovejoy1@gmail.com

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#include <string>
#include <fstream>
#include <mutex>

#include "version.h"
#include "main.h"

#define LOG_LEVEL_ERR 1
#define LOG_LEVEL_WARN 2
#define LOG_LEVEL_INFO 3

class tlkr::log
{
    public:
        log(std::string filename = "tlkr.log", bool printToConsole = false);
        ~log();
        bool printf(int loglevel, std::string message);
        bool getStatus();

    private:
        bool fPrintToConsole;
        bool status;
        std::ofstream logfile;
        std::mutex logfilemutex;
};

#endif // LOG_H_INCLUDED
