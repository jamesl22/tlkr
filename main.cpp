/* main.cpp - Part of TLKR, the decentralised messaging system
   Copyright (C) 2015  James Lovejoy  jameslovejoy1@gmail.com

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <algorithm>
#include <iostream>
#include <ctime>
#include <sstream>
#include <jsonrpccpp/server/connectors/httpserver.h>


#include "network.h"
#include "log.h"
#include "crypto.h"
#include "abstractstubserver.h"

tlkr *TLKR;

tlkr::tlkr()
{
    //Create a new log object so we can write to the log file
    Log = new log("tlkr.log", true);

    //Check to make sure it initialised correctly
    if(Log->getStatus())
    {
        //If all went well, create a network object so we can send and receive messages
        Network = new network(Log);

        //Check to make sure it initialised correctly
        if(Network->getStatus())
        {
            //Create a new crypto object so we can encrypt, decrypt, hash, sign and verify messages
            Log->printf(LOG_LEVEL_INFO, "Initialising a crypto system");
            Crypto = new crypto(true);

            //Check to make sure it initialised correctly
            if(Crypto->getStatus())
            {
                Log->printf(LOG_LEVEL_INFO, "Initialisation successful");

                //Open the leveldb database for storing key pairs
                Log->printf(LOG_LEVEL_INFO, "Opening key database");
                leveldb::Options options;

                //Create the database if it doesn't exist
                options.create_if_missing = true;

                //Actually open the databases
                leveldb::Status keyDBStatus = leveldb::DB::Open(options, "./keyDB", &keyDB);

                //Check to make sure it initialised correctly
                if(keyDBStatus.ok())
                {
                    Log->printf(LOG_LEVEL_INFO, "Opened key database successfully");

                    //Start the message handling thread
                    messageThread = new std::thread(&tlkr::handleMessages, this);
                }
                else
                {
                    Log->printf(LOG_LEVEL_ERR, "Failed to open key database");
                }
            }
            else
            {
                Log->printf(LOG_LEVEL_ERR, "Initialisation failed");
            }
        }
    }
}

tlkr::~tlkr()
{
    //Check to see if the messageThread actually exists
    if(messageThread != NULL)
    {
        //If so, stop and delete it
        delete messageThread;
    }

    //First lock the crypto object to ensure no other thread is using it
    cryptoMutex.lock();

    //Check if the crypto object exists
    if(Crypto != NULL)
    {
        //If so, delete it
        delete Crypto;
    }

    //Unlock the mutex. Although it has been deleted now and is thus useless
    cryptoMutex.unlock();

    //Check if the network object exists
    if(Network != NULL)
    {
        //If so, delete it
        delete Network;
    }


    //Lock the keyDB mutex to ensure no other thread is using it
    keyDBMutex.lock();

    //Check if the keyDB is currently open
    if(keyDB != NULL)
    {
        //If so, close it
        delete keyDB;
    }

    //Unlock the mutex. Although it has now been closed and is thus useless
    keyDBMutex.unlock();


    //Check if the log object exists
    if(Log != NULL)
    {
        //If so, close it
        delete Log;
    }
}

tlkr::getInfoStruct tlkr::getInfo()
{
    //Create the getInfoStruct that we will be retuning later
    getInfoStruct returning;

    //Assume everything went well unless we tell it otherwise
    returning.result = true;

    //Set the version to the one defined in version.h
    returning.version = version;

    //Set the current number of network connections
    returning.connections = Network->getConnections();


    //Create a string to store the number of accounts
    std::string value;

    //Gain a lock on the keyDB so that we can use it safely
    keyDBMutex.lock();

    //Get the number of accounts from the database
    leveldb::Status status = keyDB->Get(leveldb::ReadOptions(), "accounts", &value);

    //Free the keyDB so other threads can use it
    keyDBMutex.unlock();

    //Check if there was an error getting the account count
    if(status.ok())
    {
        //Set the number of accounts currently in the database
        returning.accounts = std::stoi(value);
    }
    else
    {
        Log->printf(LOG_LEVEL_ERR, "Failed to get account count from key database");
        returning.result = false;
    }

    //Get a lock on the message database so we can use it safely
    messageDBMutex.lock();

    //Set the number of messages in the database
    returning.messages = messageDB.size();

    //Unlock the mutex so other threads can use it
    messageDBMutex.unlock();


    //Return the information to the calling procedure
    return returning;
}

tlkr::account tlkr::newAccount(std::string accountName)
{
    Log->printf(LOG_LEVEL_INFO, "Generating a new keypair");

    //Create a new account variable for returning data
    account returning;

    //Get a lock on the keyDB so we can use it safely
    keyDBMutex.lock();

    //Check if the calling function provided an account name
    if(accountName != "")
    {
        //Set the account name
        returning.name = accountName;

        //This is a new account so it has 0 messages
        returning.messages = 0;

        //Create a string for storing the public key
        std::string value;

        //Get the public key from the database
        leveldb::Status status = keyDB->Get(leveldb::ReadOptions(), std::string(accountName + "_public"), &value);

        //Check if the account name already exists
        if(value == "" || !status.ok())
        {
            //Create a new crypto object and have it generate a new key pair
            crypto *keygenCrypto;
            keygenCrypto = new crypto(true);

            //Ensure it initialised correctly
            if(keygenCrypto->getStatus())
            {
                //Set the public key and make sure it isn't blank
                if((returning.publicKey = keygenCrypto->getPublicKey()) != "")
                {
                    //Write the public key to the database
                    status = keyDB->Put(leveldb::WriteOptions(), std::string(accountName + "_public"), returning.publicKey);

                    //Check this went okay
                    if(status.ok())
                    {
                        //Write the private key to the database
                        status = keyDB->Put(leveldb::WriteOptions(), std::string(accountName + "_private"), keygenCrypto->getPrivateKey());

                        //Check this went okay
                        if(status.ok())
                        {
                            //Pull the number of accounts from the database
                            keyDB->Get(leveldb::ReadOptions(), "accounts", &value);

                            //If the value is blank, this if the first account
                            if(value == "")
                            {
                                //Set the number of accounts to 1
                                status = keyDB->Put(leveldb::WriteOptions(), "accounts", "1");

                                //Check this went okay
                                if(status.ok())
                                {
                                    returning.result = true;
                                    Log->printf(LOG_LEVEL_INFO, "New keypair generated successfully");
                                }
                                else
                                {
                                    returning.result = false;
                                    Log->printf(LOG_LEVEL_ERR, "Could not write account count to key database");
                                }
                            }
                            else
                            {
                                //Create an integer from the string to store the number of accounts
                                int accounts = std::stoi(value);

                                //Increment it
                                accounts++;

                                //Delete the account count from the database
                                status = keyDB->Delete(leveldb::WriteOptions(), "accounts");

                                //Check this went okay
                                if(status.ok())
                                {
                                    //Write the new account count back to the database
                                    status = keyDB->Put(leveldb::WriteOptions(), "accounts", std::to_string(accounts));

                                    //Check this went okay
                                    if(status.ok())
                                    {
                                        returning.result = true;
                                        Log->printf(LOG_LEVEL_INFO, "New keypair generated successfully");
                                    }
                                    else
                                    {
                                        returning.result = false;
                                        Log->printf(LOG_LEVEL_ERR, "Could not write account count to key database");
                                    }
                                }
                                else
                                {
                                    returning.result = false;
                                    Log->printf(LOG_LEVEL_ERR, "Could not write account count to key database");
                                }
                            }
                        }
                        else
                        {
                            Log->printf(LOG_LEVEL_ERR, "Failed to add new keypair to key database");
                            returning.result = false;
                        }
                    }
                    else
                    {
                        Log->printf(LOG_LEVEL_ERR, "Failed to add new keypair to key database");
                        returning.result = false;
                    }
                }
                else
                {
                    Log->printf(LOG_LEVEL_ERR, "Failed to generate a keypair");
                    returning.result = false;
                }
            }
            else
            {
                Log->printf(LOG_LEVEL_ERR, "Failed to intialise a crypto system");
                returning.result = false;
            }

            //Delete the crypto object as we're finished with it
            delete keygenCrypto;
        }
        else
        {
            Log->printf(LOG_LEVEL_ERR, "Account name already exists in key database or db query failed");
            returning.result = false;
        }
    }
    else
    {
        Log->printf(LOG_LEVEL_ERR, "No account name provided");
        returning.result = false;
    }

    //Unlock the keyDB so other threads can use it
    keyDBMutex.unlock();

    //Return the new account to the calling function
    return returning;
}

tlkr::listAccountsStruct tlkr::listAccounts()
{
    //Create a listAccountsStruct to return to the user
    listAccountsStruct returning;

    //Assume everything went well unless we tell it otherwise
    returning.result = true;

    //Lock the keyDB so we can use it safely
    keyDBMutex.lock();

    //Create a new string to store the account count
    std::string value;

    //Get the account count from the database
    leveldb::Status status = keyDB->Get(leveldb::ReadOptions(), "accounts", &value);

    //Check this went okay and that the account count is greater than 0
    if(status.ok() || value != "")
    {
        //Create a new iterator for searching the database
        leveldb::Iterator *it = keyDB->NewIterator(leveldb::ReadOptions());

        //Iterate over every key in the database
        for(it->SeekToFirst(); it->Valid(); it->Next())
        {
            //Put the key and value into their own strings
            leveldb::Slice key = it->key();
            std::string key_str = key.ToString();
            leveldb::Slice value = it->value();
            std::string value_str = value.ToString();

            //Check if the value is a public key
            if(key_str.substr(key_str.length() - 7, key_str.length()) == "_public")
            {
                //Create a temporary account object to add to the accounts vector
                account tempAccount;

                //Assume all went well unless we tell it otherwise
                tempAccount.result = true;

                //Set the account name
                tempAccount.name = key_str.substr(0, key_str.length() - 7);

                //Initially the number of account messages is 0
                tempAccount.messages = 0;

                //Get a lock on the message database so we can use it safely
                messageDBMutex.lock();

                //Iterate over the message database to find messages that belong to this account
                for(unsigned int i = 0; i < messageDB.size(); i++)
                {
                    //Check if the message belongs to this account
                    if(messageDB[i].accountName == tempAccount.name)
                    {
                        //Increment the number of account messages
                        tempAccount.messages++;
                    }
                }

                //Unlock the message database so other threads can use it
                messageDBMutex.unlock();

                //Set the public key
                tempAccount.publicKey = value_str;

                //Add the temporary account to the accounts vector
                returning.accounts.push_back(tempAccount);
            }
        }

        //Delete the iterator as we have finished using it
        delete it;
    }
    else
    {
        returning.result = false;
        Log->printf(LOG_LEVEL_WARN, "There are currently no accounts in the database");
    }

    //Unlock the key database to other threads can use it
    keyDBMutex.unlock();

    //Return the list of accounts to the calling function
    return returning;
}

tlkr::account tlkr::getAccountInfo(std::string accountName)
{
    //Create a new account object to return to the calling function
    account returning;

    //Check that the calling function provided an account name
    if(accountName != "")
    {
        //Create a new string to store the public key
        std::string value;

        //Get a lock on the key database so we can use it safely
        keyDBMutex.lock();

        //Get the public key from the database
        leveldb::Status status = keyDB->Get(leveldb::ReadOptions(), std::string(accountName + "_public"), &value);

        //Check that this went okay
        if(status.ok())
        {
            //Set the number of messages to 0 initially
            returning.messages = 0;

            //Get a lock on the message database so we can use it safely
            messageDBMutex.lock();

            //Iterate through the message database to find messages that belong to this account
            for(unsigned int i = 0; i < messageDB.size(); i++)
            {
                //Check that the message belongs to this account
                if(messageDB[i].accountName == accountName)
                {
                    //Increment the number of account messages
                    returning.messages++;
                }
            }

            //Unlock the database mutex so that other threads can use it
            messageDBMutex.unlock();

            //Set the account name
            returning.name = accountName;

            //Set the public key
            returning.publicKey = value;

            //Everything went well, set the result to true
            returning.result = true;
        }
        else
        {
            returning.result = false;
        }

        //Unlock the key database so other threads can use it
        keyDBMutex.unlock();
    }
    else
    {
        returning.result = false;
    }

    //Return the account to the calling function
    return returning;
}

tlkr::message tlkr::sendMessage(std::string accountName, std::string recipient, std::string messageString)
{
    //Create a new message object to return to the calling function
    message returning;

    //Check if the calling function provided an account name, public key and message
    if(accountName != "" && recipient != "" && messageString != "")
    {
        //Lock the key database so we can use it safely
        keyDBMutex.lock();

        //Create a new string to store the private key
        std::string privateKey;

        //Get the private key from the database
        leveldb::Status status = keyDB->Get(leveldb::ReadOptions(), std::string(accountName + "_private"), &privateKey);

        //Check that went okay
        if(status.ok())
        {
            //Unlock the key database
            keyDBMutex.unlock();

            //Lock the crypto object so we can use it safely
            cryptoMutex.lock();

            //Set the private key of the crypto object
            if(Crypto->setPrivateKey(privateKey))
            {
                //Generate a signature from the message
                std::string signature = Crypto->sign(messageString);

                //Store the public key of the account
                std::string publicKey = Crypto->getPublicKey();

                //Check that we generated a signature successfully
                if(signature != "")
                {
                    //Set the crypto object to the public key of the recipient
                    if(Crypto->setPublicKey(recipient))
                    {
                        //Concatenate the plain text message to be sent
                        std::stringstream stagingString;
                        stagingString << messageString << static_cast<char>(0x1D) << signature << static_cast<char>(0x1D) << publicKey;

                        //Encrypt the plain text with the recipient public key
                        std::string cipherText = Crypto->encrypt(stagingString.str());

                        //Check that we encrypted it correctly
                        if(cipherText != "")
                        {
                            stagingString.str("");
                            stagingString << cipherText << "=>" << Crypto->getEk();
                            //Broadcast the message on the network
                            if(Network->sendMessage(stagingString.str()))
                            {
                                //Create a temporary string to store the hash
                                std::string temp;

                                //Hash the cipher text using SHA256
                                temp = Crypto->sha256(stagingString.str());

                                //Lock the hash database we can use it safely
                                hashDBMutex.lock();

                                //Add the hash to the database
                                hashDB.push_back(temp);

                                //Unlock the hash database so other threads can use it
                                hashDBMutex.unlock();

                                //Set the account name
                                returning.accountName = accountName;

                                //Set the message string
                                returning.messageString = messageString;

                                //Set the recipient public key
                                returning.recipient = recipient;

                                //This message belongs to use since we sent it
                                returning.isMine = true;

                                //Everything went well so set result to true
                                returning.result = true;

                                //Lock the message database so we can use it safely
                                messageDBMutex.lock();

                                //Add the message to the message database
                                messageDB.push_back(returning);

                                //Unlock the database so other threads can use it
                                messageDBMutex.unlock();
                            }
                            else
                            {
                                Log->printf(LOG_LEVEL_ERR, "Failed to send message");
                                returning.result = false;
                            }
                        }
                        else
                        {
                            Log->printf(LOG_LEVEL_ERR, "Failed to encrypt message");
                            returning.result = false;
                        }
                    }
                    else
                    {
                        Log->printf(LOG_LEVEL_ERR, "Failed to set recipient public key");
                        returning.result = false;
                    }
                }
                else
                {
                    Log->printf(LOG_LEVEL_ERR, "Failed to sign message");
                    returning.result = false;
                }
            }
            else
            {
                Log->printf(LOG_LEVEL_ERR, "Failed to set private key");
                returning.result = false;
            }
            //Unlock the crypto object since we're finished using it
            cryptoMutex.unlock();
        }
        else
        {
            keyDBMutex.unlock();
            Log->printf(LOG_LEVEL_ERR, "Failed to get private key from database");
            returning.result = false;
        }
    }
    else
    {
        returning.result = false;
    }
    //Return the message to the calling function
    return returning;
}

void tlkr::handleMessages()
{
    //Loop constantly while the thread is running
    while(true)
    {
        //Get a message from the message queue
        std::string messageString = Network->popMessage();

        //Check if there were any messages in the queue
        if(messageString != "")
        {
            //Create a string to store the hash of the received message
            std::string messageHash;

            //Hash the received message
            messageHash = Crypto->sha256(messageString);

            //Create an iterator for searching the hash database
            std::vector<std::string>::iterator iter;

            //Lock the hash database so we can use it safely
            hashDBMutex.lock();

            //Search the database for the message hash to see if we received it already
            iter = std::find(hashDB.begin(), hashDB.end(), messageHash);

            //Check if we found it
            if(iter == hashDB.end())
            {
                //If not, add the hash to the database
                hashDB.push_back(messageHash);

                //Unlock the hash database so other threads can use it
                hashDBMutex.unlock();

                //Rebroadcast the message on the network
                Network->sendMessage(messageString);

                //Lock the key database so we can use it safely
                keyDBMutex.lock();

                //Create a new iterator for searching the key database
                leveldb::Iterator *it = keyDB->NewIterator(leveldb::ReadOptions());

                //Iterate over the database to find a key pair which decrypts the message
                for(it->SeekToFirst(); it->Valid(); it->Next())
                {
                    //Store the key and value in their own strings
                    leveldb::Slice key = it->key();
                    std::string key_str = key.ToString();
                    leveldb::Slice value = it->value();
                    std::string value_str = value.ToString();

                    //Check if the value is a private key
                    if(key_str.substr(key_str.length() - 8, key_str.length()) == "_private")
                    {
                        //Lock the crypto object so we can use it safely
                        cryptoMutex.lock();

                        //Set the private key of the crypto object
                        if(Crypto->setPrivateKey(value_str))
                        {
                            if(Crypto->setEk(messageString.substr(messageString.find("=>") + 2, messageString.size())))
                            {
                                //Attempt to decrypt the message
                                std::string plainText = Crypto->decrypt(messageString.substr(0, messageString.find("=>")));

                                //Check if this worked
                                if(plainText != "")
                                {
                                    //Serialise the string to extract the message, signature and public key
                                    std::string content = plainText.substr(0, plainText.find(static_cast<char>(0x1D)));
                                    std::string signature = plainText.substr(plainText.find(static_cast<char>(0x1D)) + 1, plainText.find("-----BEGIN PUBLIC KEY-----") - 11);
                                    std::string publicKey = plainText.substr(plainText.find("-----BEGIN PUBLIC KEY-----"));

                                    //Set the public key of the crypto system to the sender public key
                                    if(Crypto->setPublicKey(publicKey))
                                    {
                                        //Verify signature with the senders public key
                                        if(Crypto->verify(content, signature))
                                        {
                                            //Create a temporary message to add to the message database
                                            message temp;

                                            //Set account name
                                            temp.accountName = key_str.substr(0, key_str.length() - 8);

                                            //Set message string
                                            temp.messageString = content;

                                            //Set sender public key
                                            temp.recipient = publicKey;

                                            //Everything went well, set result to true
                                            temp.result = true;

                                            //Since we received it from the network, this message doesn't belong to us
                                            temp.isMine = false;

                                            //Lock messageDB so we can use it safely
                                            messageDBMutex.lock();

                                            //Add temporary message to the messageDB
                                            messageDB.push_back(temp);

                                            //Unlock the messageDB to allow other threads to use it
                                            messageDBMutex.unlock();
                                            Log->printf(LOG_LEVEL_INFO, "Detected a message intended for us");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                Log->printf(LOG_LEVEL_ERR, "Failed to set ek");
                            }
                        }
                        else
                        {
                            Log->printf(LOG_LEVEL_ERR, "Couldn't set private key");
                        }
                        cryptoMutex.unlock();
                    }
                }

                //Delete the iterator as we're done using it
                delete it;

                //Unlock the keyDB so other threads can use it
                keyDBMutex.unlock();
            }
            //Unlock the hashDB so other threads can use it
            hashDBMutex.unlock();
        }
        else
        {
            //If there weren't any messages on this loop, sleep for 100ms to give mercy to the CPU
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    }
}

tlkr::listMessagesStruct tlkr::listMessages(std::string accountName)
{
    //Create a listMessagesStruct to return to the calling function
    listMessagesStruct returning;

    //Check that the calling function provided an account name
    if(accountName != "")
    {
        //Assume we failed unless told otherwise
        returning.result = false;

        //Lock the messageDB so we can use it safely
        messageDBMutex.lock();

        //Loop over the messageDB to find messages that belong to this account
        for(unsigned int i = 0; i < messageDB.size(); i++)
        {
            //Check if the message belongs to the account
            if(messageDB[i].accountName == accountName)
            {
                //Set result to true, as we found a message
                returning.result = true;

                //Add the message to the message vector to be sent to the calling function
                returning.messages.push_back(messageDB[i]);
            }
        }

        //Unlock the messageDB to allow other threads to use it
        messageDBMutex.unlock();
    }
    else
    {
        returning.result = false;
    }

    //Return the messages to the calling function
    return returning;
}

std::string getCmdOption(char **begin, char **end, const std::string &option)
{
    char **itr = std::find(begin, end, option);
    if (itr != end && ++itr != end)
    {
        std::string returning((const char*)*itr);
        return returning;
    }
    return 0;
}

bool cmdOptionExists(char **begin, char **end, const std::string &option)
{
    return std::find(begin, end, option) != end;
}

class MyStubServer : public AbstractStubServer
{
public:
    MyStubServer(jsonrpc::AbstractServerConnector &connector);

    virtual Json::Value newaccount(const std::string& accountname);
    virtual Json::Value listaccounts();
    virtual Json::Value getaccountinfo(const std::string& accountname);
    virtual Json::Value listmessages(const std::string& accountname);
    virtual Json::Value sendmessage(const std::string& accountname, const std::string& messagestring, const std::string& recipient);
    virtual Json::Value getinfo();
};

MyStubServer::MyStubServer(jsonrpc::AbstractServerConnector &connector) : AbstractStubServer(connector)
{

}

Json::Value MyStubServer::newaccount(const std::string &accountname)
{
    tlkr::account temp = TLKR->newAccount(accountname);
    Json::Value returning;
    returning["method"] = "newaccount";
    returning["result"] = temp.result;
    returning["publickey"] = temp.publicKey;
    returning["name"] = temp.name;

    return returning;
}

Json::Value MyStubServer::listaccounts()
{
    tlkr::listAccountsStruct temp = TLKR->listAccounts();
    Json::Value returning;

    returning["method"] = "listaccounts";
    returning["result"] = temp.result;

    Json::Value accountArray;
    for(unsigned int i = 0; i < temp.accounts.size(); i++)
    {
        Json::Value account;
        account["result"] = temp.accounts[i].result;
        account["name"] = temp.accounts[i].name;
        account["publickey"] = temp.accounts[i].publicKey;
        account["messages"] = temp.accounts[i].messages;

        accountArray[i] = account;
    }

    returning["accounts"] = accountArray;

    return returning;
}

Json::Value MyStubServer::getaccountinfo(const std::string &accountname)
{
    tlkr::account temp = TLKR->getAccountInfo(accountname);
    Json::Value returning;

    returning["method"] = "getaccountinfo";
    returning["result"] = temp.result;
    returning["name"] = temp.name;
    returning["publickey"] = temp.publicKey;
    returning["messages"] = temp.messages;

    return returning;
}

Json::Value MyStubServer::listmessages(const std::string &accountname)
{
    tlkr::listMessagesStruct temp = TLKR->listMessages(accountname);
    Json::Value returning;

    returning["method"] = "listmessages";
    returning["result"] = temp.result;
    returning["account"] = accountname;
    returning["publickey"] = TLKR->getAccountInfo(accountname).publicKey;

    Json::Value messageArray;
    for(unsigned int i = 0; i < temp.messages.size(); i++)
    {
        Json::Value message;
        message["result"] = temp.messages[i].result;
        if(temp.messages[i].isMine)
        {
            message["recipient"] = temp.messages[i].recipient;
        }
        else
        {
            message["sender"] = temp.messages[i].recipient;
        }
        message["ismine"] = temp.messages[i].isMine;
        message["messagestring"] = temp.messages[i].messageString;

        messageArray[i] = message;
    }

    returning["messages"] = messageArray;

    return returning;
}

Json::Value MyStubServer::sendmessage(const std::string &accountname, const std::string &messagestring, const std::string &recipient)
{
    tlkr::message temp = TLKR->sendMessage(accountname, recipient, messagestring);
    Json::Value returning;

    returning["method"] = "sendmessage";
    returning["result"] = temp.result;
    returning["accountname"] = temp.accountName;
    returning["recipient"] = temp.recipient;
    returning["messagestring"] = temp.messageString;

    return returning;
}

Json::Value MyStubServer::getinfo()
{
    tlkr::getInfoStruct temp = TLKR->getInfo();
    Json::Value returning;

    returning["method"] = "getinfo";
    returning["result"] = temp.result;
    returning["version"] = temp.version;
    returning["connections"] = temp.connections;
    returning["accounts"] = temp.accounts;
    returning["messages"] = temp.messages;

    return returning;
}

int main(int argc, char *argv[])
{
    if(cmdOptionExists(argv, argv + argc, "-v") || cmdOptionExists(argv, argv + argc, "--version"))
    {
        std::cout << "TLKR version " << version << "\n";
    }
    else if(cmdOptionExists(argv, argv + argc, "-h") || cmdOptionExists(argv, argv + argc, "--help"))
    {
        std::cout << "TLKR version " << version << "\n\n";
        std::cout << "-v, --version    Display version information\n";
        std::cout << "-h, --help       Display this help message\n";
        std::cout << "--daemon         Start an instance of the TLKR daemon\n";
    }
    else if(cmdOptionExists(argv, argv + argc, "--daemon"))
    {
        TLKR = new tlkr;

        jsonrpc::HttpServer httpserver(42070);
        MyStubServer s(httpserver);
        s.StartListening();
        while(true)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        }
        s.StopListening();

        delete TLKR;
    }

    return 0;
}
