#!/bin/sh
rm -r *.o
rm -r tlkr
g++ -std=c++11 -Wall -g -c log.cpp -o log.o
g++ -std=c++11 -Wall -g -c network.cpp -o network.o
g++ -std=c++11 -Wall -g -c main.cpp -o main.o
g++ -std=c++11 -Wall -g -c crypto.cpp -o crypto.o
g++ -std=c++11 -Wall -g -c base64.cpp -o base64.o
g++ -o tlkr log.o main.o network.o crypto.o base64.o -lpthread -lenet -lcrypto -lleveldb -ljsonrpccpp-server -ljsonrpccpp-common -ljsoncpp -lmicrohttpd
