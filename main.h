/* main.h - Part of TLKR, the decentralised messaging system
   Copyright (C) 2015  James Lovejoy  jameslovejoy1@gmail.com

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#include <leveldb/db.h>
#include <mutex>
#include <vector>
#include <thread>

class tlkr
{
    public:
        tlkr();
        ~tlkr();
        struct getInfoStruct
        {
            bool result;
            std::string version;
            int connections;
            int accounts;
            int messages;
        };
        struct account
        {
            bool result;
            std::string name;
            std::string publicKey;
            int messages;
        };
        struct listAccountsStruct
        {
            bool result;
            std::vector<account> accounts;
        };
        struct message
        {
            bool result;
            std::string accountName;
            std::string recipient;
            std::string messageString;
            bool isMine;
        };
        struct listMessagesStruct
        {
            bool result;
            std::vector<message> messages;
        };
        getInfoStruct getInfo();
        account newAccount(std::string accountName);
        listAccountsStruct listAccounts();
        account getAccountInfo(std::string accountName);
        message sendMessage(std::string accountName, std::string recipient, std::string messageString);
        listMessagesStruct listMessages(std::string accountName);

    private:
        class log;
        class network;
        class crypto;
        void handleMessages();
        std::thread *messageThread;
        network *Network;
        leveldb::DB *keyDB;
        crypto *Crypto;
        std::vector<std::string> hashDB;
        std::vector<message> messageDB;
        log *Log;
        std::mutex keyDBMutex;
        std::mutex hashDBMutex;
        std::mutex messageDBMutex;
        std::mutex cryptoMutex;
};

#endif // MAIN_H_INCLUDED
