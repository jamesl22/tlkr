 /* log.cpp - Part of TLKR, the decentralised messaging system
    Copyright (C) 2015  James Lovejoy  jameslovejoy1@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <ctime>
#include <chrono>
#include <sstream>
#include <iostream>

#include "log.h"

tlkr::log::log(std::string filename, bool printToConsole)
{
    fPrintToConsole = printToConsole;
    logfile.open(filename, std::ios::app);
    if(logfile.is_open())
    {
        logfile << "\n\n\n\n\n";
        printf(LOG_LEVEL_INFO, "Tlkr version " + version + " started");
        status = true;
    }
    else
    {
        status = false;
    }
}

tlkr::log::~log()
{
    logfilemutex.lock();
    logfile.close();
    logfilemutex.unlock();
}

bool tlkr::log::printf(int loglevel, std::string message)
{
    std::chrono::system_clock::time_point today = std::chrono::system_clock::now();
    time_t tt = std::chrono::system_clock::to_time_t(today);
    std::string t(ctime(&tt));
    std::ostringstream stagingstream;
    stagingstream << t.substr(0, t.length() - 1) << " ";

    switch(loglevel)
    {
        case LOG_LEVEL_ERR:
            stagingstream << "ERROR ";
            break;

        case LOG_LEVEL_WARN:
            stagingstream << "WARNING ";
            break;

        case LOG_LEVEL_INFO:
            stagingstream << "INFO ";
            break;

        default:
            return false;
            break;
    }

    stagingstream << message << "\n";

    if(fPrintToConsole)
    {
        std::cout << stagingstream.str();
    }

    logfilemutex.lock();
    logfile << stagingstream.str();
    logfile.flush();
    logfilemutex.unlock();

    return true;
}

bool tlkr::log::getStatus()
{
    return status;
}
