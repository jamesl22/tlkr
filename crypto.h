/* crypto.h - Part of TLKR, the decentralised messaging system
   Copyright (C) 2015  James Lovejoy  jameslovejoy1@gmail.com

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef CRYPTO_H_INCLUDED
#define CRYPTO_H_INCLUDED

#include <openssl/evp.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <string>

#include "main.h"

#define RSA_KEYLEN 2048

class tlkr::crypto
{
    public:
        crypto(bool fGenerate = false);
        ~crypto();
        bool getStatus();
        std::string decrypt(std::string cipherText);
        std::string encrypt(std::string plainText);
        std::string sign(std::string message);
        bool verify(std::string message, std::string signature);
        std::string getPublicKey();
        std::string getPrivateKey();
        bool setPublicKey(std::string publicKey);
        bool setPrivateKey(std::string privateKey);
        std::string sha256(std::string message);
        std::string getEk();
        bool setEk(std::string Ek);

    private:
        EVP_PKEY *keypair;
        EVP_CIPHER_CTX *rsaCtx;
        bool status;
        unsigned char *ek;
        int ekl;
        unsigned char *iv;
        int ivl;
};

#endif // CRYPTO_H_INCLUDED
